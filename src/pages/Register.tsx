import { IonCheckbox, IonContent, IonHeader, IonPage, IonToolbar, IonDatetime, IonButtons, IonBackButton, IonPopover, IonLoading, IonAlert } from '@ionic/react';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Storage from '../services/storage';
import Variables from '../variables';

const Register: React.FC<any> = (props) => {
    const [showLoading, setShowLoading] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [showPopover, setShowPopover] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [date, setDate] = useState(new Date());
    const [header, setHeader] = useState('');
    const [mensagem, setMensagem] = useState('');

    let termos = {
        name: 'agreement', isChecked: false
    }

    function validateAge(birthDate: Date) {
        const nowDate = new Date();
        var age = nowDate.getFullYear() - birthDate.getFullYear();;
        if (age >= 15)
            return true
        else
            return false
    }

    function validateForm() {
        if (name == "") {
            setMensagem("Por favor preencha seu nome");
            setShowPopover(true)
        } else if (email == "") {
            setMensagem("Por favor preencha seu e-mail");
            setShowPopover(true);
        } else if (password == "") {
            setMensagem("Por favor preencha sua senha");
            setShowPopover(true)
        } else if (password.length < 6) {
            setMensagem("Senha muito fraca! Por favor escolha uma senha com no mínimo 6 caracteres");
            setShowPopover(true)
        } else if (!date) {
            setMensagem("Por favor preencha sua data de aniversário");
            setShowPopover(true)
        } else if (!validateAge(date)) {
            setMensagem("Para se cadastrar é necessário ter mais de 15 anos");
            setShowPopover(true)
        } else if (!termos.isChecked) {
            setMensagem("É necessário concordar com os termos de serviço");
            setShowPopover(true)
        } else {
            setShowLoading(true)
            try {
                axios({
                    method: 'post',
                    url: 'http://192.168.0.125/projetos/hivefy/api/Cadastrar/usuario/',
                    data: `nome=${name}&instituicao=1&senha=${password}&email=${email}&nascimento=${date.toISOString().split('T')[0]}`
                })
                    .then(function (response) {
                        setShowLoading(false);
                        console.log(response.data);
                        if (response.data.status == "success") {
                            setShowLoading(false);
                            setHeader("Cadastro realizado com sucesso!");
                            setMensagem("Realize seu login e descubra suas coméias.")
                            console.log("Cadastrado com sucesso!");
                        } else if (response.data.status == "conflict") {
                            setShowLoading(false);
                            setHeader("Problema ao realizar cadastro");
                            setMensagem("Já existe um usuário cadastrado com este e-mail!")
                            console.log("Já existe um usuário cadastrado com este e-mail!");
                        } else if (response.data.status == "nullentry") {
                            setShowLoading(false);
                            setHeader("Problema ao realizar cadastro");
                            setMensagem("Houve um problema ao cadastrar o usuário com os dados fornecidos!")
                            console.log("Por favor complete os dados para se cadastrar!");
                        }
                    })
                    .catch(function (error) {
                        setShowLoading(false);
                        setHeader("Problema ao realizar cadastro");
                        setMensagem("Houve um problema ao cadastrar o usuário. Tente novamente mais tarde");
                        console.log(error);
                    })
                    .finally(function () {
                        setShowAlert(true);
                    });

            } catch (error) {

            }
        }
    }

    return (
        <IonPage>
            <IonHeader className="mu-top" color="medium" >
                <IonToolbar className="mu-top">
                    <IonButtons slot="start">
                        <IonBackButton text="Voltar" />
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent className="ion-padding">
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Registrando!'}
                />
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    header={header}
                    message={mensagem}
                    buttons={[
                        {
                            text: 'Confrimar',
                            handler: () => {
                                setShowLoading(false);
                                Storage.removeStorageItem('token');
                                props.history.replace('/login');
                            }
                        }
                    ]}
                />
                <div className="container head flexy">
                    <div className="mu-body gridy">
                        <h1 className="mu-title">REGISTRE-SE</h1>
                        <IonPopover isOpen={showPopover} onDidDismiss={e => setShowPopover(false)}>
                            <div className="mu-body text-center"><p>{mensagem}</p></div>
                        </IonPopover>
                        <form className="gridy" action="" autoComplete="off">
                            <label>Nome Completo</label>
                            <input type="text" className="mu-input-large" placeholder="Nome" name="nome" onChange={event => { setName(event.target.value) }} required autoComplete="off"></input>
                            <div className="espaco10"></div>
                            <label>E-mail</label>
                            <input type="email" className="mu-input-large" placeholder="E-mail" name="email" onChange={event => { setEmail(event.target.value) }} required autoComplete="off"></input>
                            <div className="espaco10"></div>
                            <label>Senha</label>
                            <input type="password" className="mu-input-large" placeholder="Senha" name="senha" onChange={event => { setPassword(event.target.value) }} required autoComplete="off"></input>
                            <div className="espaco10"></div>
                            <label>Data de Nascimento</label>
                            <IonDatetime className="mu-input-large datetime-text" pickerOptions={{
                                buttons: [
                                    {
                                        text: 'Concluir',
                                        handler: (event) =>  {
                                                console.log(event);
                                                try {
                                                    setDate(new Date(event.year.value, event.month.value-1, event.day.value))
                                                } catch (error) {
                                                    
                                                }
                                            }
                                    }, {
                                        text: 'Cancelar',
                                        handler: () => {
                                        }
                                    }
                                ]
                            }} displayFormat="DD/MM/YYYY" placeholder="DD/MM/AAAA"></IonDatetime>
                            <div className="espaco25"></div>
                            <div className="centerfy">
                                <IonCheckbox color="danger" onClick={() => { termos.isChecked = !termos.isChecked }} value={termos.name} />
                                <br></br>

                                Concordo e afirmo que li os
                                <br></br><a href="https://google.com.br">Termos de Serviço</a>
                            </div>
                            <div className="espaco25"></div>
                            <button type="submit" id="muAccess" className="mu-button" onClick={(e) => {
                                e.preventDefault(); validateForm();
                            }}><h4>CADASTRAR</h4></button>
                        </form>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    );
};

export default Register;

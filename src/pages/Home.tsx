import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, withIonLifeCycle, IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel, IonBadge, IonRouterOutlet } from '@ionic/react';
import React from 'react';
import axios from 'axios';
import Storage from '../services/storage';
import Validation from '../services/validation';
import { apps, flash, send, home, contacts, search, person } from 'ionicons/icons';
import { Route, Redirect } from 'react-router';
import Start from './Home/Main';
import Hives from './Home/Hives';
import Search from './Home/Search';
import Profile from './Home/Profile';
import { IonReactRouter } from '@ionic/react-router';

class Home extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = { token: null };
  }

  mirrorTo(screen: string | null) {
    if (screen != "" || screen != null)
      this.props.history.push(screen);
  }

  ionViewWillEnter() {
    let outer = this;
    Validation.validateToken().then((resultado) => {
      if (resultado) {

      } else {
        outer.mirrorTo("/login")
      }
    })
  }

  render() {
    return (
      <IonPage id="main">
        <IonReactRouter>
          <IonTabs>
            <IonRouterOutlet>
              <Route path="/home/tab1" component={Start} />
              <Route path="/home/tab2" component={Hives} />
              <Route path="/home/tab3" component={Search} />
              <Route path="/home/tab4" component={Profile} />
              <Route path="/home/" render={() => <Redirect to="/home/tab1" />} exact={true} />
            </IonRouterOutlet>
            <IonTabBar slot="bottom">
              <IonTabButton tab="/home/tab1" href="/home/tab1">
                <IonIcon icon={home} />
                <IonLabel>Início</IonLabel>
              </IonTabButton>
              <IonTabButton tab="/home/tab2" href="/home/tab2">
                <IonIcon icon={contacts} />
                <IonLabel>Coméias</IonLabel>
              </IonTabButton>
              <IonTabButton tab="/home/tab3" href="/home/tab3">
                <IonIcon icon={search} />
                <IonLabel>Busca</IonLabel>
              </IonTabButton>
              <IonTabButton tab="/home/tab4" href="/home/tab4">
                <IonIcon icon={person} />
                <IonLabel>Perfil</IonLabel>
              </IonTabButton>
            </IonTabBar>
          </IonTabs>
        </IonReactRouter>
      </IonPage>
    );
  }
};

export default withIonLifeCycle(Home);

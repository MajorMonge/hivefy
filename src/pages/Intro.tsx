import { IonContent, IonPage, IonSlides, IonSlide, withIonLifeCycle } from '@ionic/react';
import React from 'react';
import Storage from '../services/storage';

class Intro extends React.Component<any, any>  {

    constructor(props: any) {
        super(props);
    }

    ionViewWillEnter() {
        console.log("Will Enter")
        let outer = this;
        Promise.resolve(Storage.getToken()).then(function (token) {
            if (token.value != null && token.value != undefined) {
                outer.mirrorTo('/login')
                console.log(token.value); // "Success"
            } else {
                console.log("No token avaiable"); // "No token"
            }
        }, function (value) {
            // not called
        });
    }


    mirrorTo(screen: string | null) {
        if (screen != "" || screen != null)
            this.props.history.push(screen);
    }

    render() {

        const slideOpts = {
            initialSlide: 0
        };

        return (
            <IonPage>
                <IonContent className="main">
                    <IonSlides pager={true} options={slideOpts}>
                        <IonSlide>
                            <div className="container head flexy">
                                <div className="mu-header flexy">
                                    <h1 className="mu-title">HIVEFY</h1>
                                    <h2 className="mu-description">Conexão estudantil nunca foi tão fácil</h2>
                                </div>
                                <div className="mu-cover">
                                </div>
                                <div className="espaco30"></div>

                                <div className="container">
                                    <div className="mu-body gridy">
                                        <h1 className="mu-title">ESTUDOS E GRUPOS, EM UM LUGAR SÓ</h1>
                                        <div className="espaco25"></div>
                                        <h5 className="mu-summary text-justify">Lorem ipsum, dolor sit amet consectetur adipisicing elit. <br></br>Nobis eum vero veritatis excepturi. Nobis eum vero veritatis excepturi.Nobis eum vero veritatis excepturi.</h5>
                                        <div className="espaco20"></div>
                                        <span className="icon icon-studying"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span><span className="path16"></span><span className="path17"></span><span className="path18"></span><span className="path19"></span><span className="path20"></span><span className="path21"></span></span>
                                    </div>
                                </div>
                                <div className="espaco20"></div>
                            </div>
                        </IonSlide>
                        <IonSlide>
                            <div className="container head flexy">
                                <div className="mu-header flexy" style={{ backgroundImage: `url(${require("../assets/images/stock.jpg")})` }}>
                                    <h1 className="mu-title">HIVEFY</h1>
                                </div>
                                <div className="mu-cover">
                                </div>
                                <div className="espaco20"></div>

                                <div className="container">
                                    <div className="mu-body gridy">
                                        <span className="icon icon-parents"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span><span className="path16"></span><span className="path17"></span><span className="path18"></span><span className="path19"></span><span className="path20"></span><span className="path21"></span><span className="path22"></span><span className="path23"></span><span className="path24"></span><span className="path25"></span><span className="path26"></span><span className="path27"></span><span className="path28"></span><span className="path29"></span><span className="path30"></span><span className="path31"></span><span className="path32"></span></span>
                                        <div className="espaco25"></div>
                                        <h1 className="mu-title">DESCUBRA E APRENDA EM CONJUNTO</h1>
                                        <div className="espaco25"></div>
                                        <h5 className="mu-summary text-left">O Hivefy te conecta mundiamente a grupos de estudos. Lorem, ipsum dolor sit amet consectetur adipisicing elit. </h5>
                                        <br></br>

                                    </div>
                                </div>
                                <div className="espaco20"></div>
                            </div>
                        </IonSlide>
                        <IonSlide style={{ height: "100vh" }}>
                            <div className="container head flexy">
                                <div className="container">
                                    <div className="mu-body gridy">
                                        <div className="espaco30"></div>
                                        <span className="icon icon-chat"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span><span className="path12"></span><span className="path13"></span><span className="path14"></span><span className="path15"></span><span className="path16"></span><span className="path17"></span><span className="path18"></span><span className="path19"></span><span className="path20"></span><span className="path21"></span><span className="path22"></span><span className="path23"></span><span className="path24"></span><span className="path25"></span><span className="path26"></span><span className="path27"></span><span className="path28"></span><span className="path29"></span><span className="path30"></span><span className="path31"></span><span className="path32"></span><span className="path33"></span><span className="path34"></span><span className="path35"></span><span className="path36"></span></span>
                                        <div className="espaco25"></div>
                                        <h1 className="mu-title">CONFIRA O QUE ESTÁ ROLANDO</h1>
                                        <div className="espaco25"></div>
                                        <h5 className="mu-summary text-center">O Hivefy possui uma interface amigável e prática para você se atualizar com seus grupos de estudo. </h5>
                                        <br></br>
                                        <span className="icon icon-big-data"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span><span className="path8"></span><span className="path9"></span><span className="path10"></span><span className="path11"></span></span>
                                        <div className="espaco25"></div>
                                        <h1 className="mu-title">CONECTE-SE</h1>
                                        <div className="espaco25"></div>
                                        <h5 className="mu-summary text-center">Descubra novos grupos de estudo de seu interesse a nível mundial! Ideal para aqueles sedentos por conhecimento.</h5>
                                        <div className="espaco40"></div>
                                        <div className="flexy">
                                            <button type="submit" id="muAccess" className="mu-button bottomfy" onClick={(e) => {
                                                e.preventDefault(); this.mirrorTo('/login');
                                            }}><h4>COMEÇAR</h4></button>
                                        </div>
                                    </div>
                                </div>
                                <div className="espaco20"></div>
                            </div>
                        </IonSlide>
                    </IonSlides>
                </IonContent>
            </IonPage>
        );
    }
};

export default withIonLifeCycle(Intro);

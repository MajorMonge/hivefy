import { IonContent, IonPage, withIonLifeCycle, IonLoading, IonPopover, IonAlert } from '@ionic/react';
import React from 'react';
import Storage from '../services/storage';
import Variables from '../variables';
import axios from 'axios';

class Login extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = { uid: null, token: null, showLoading: false, showAlert: false, showPopoverEmail: false, showPopoverPassword: false, email: '', password: '', message: '' };
    }

    mirrorTo(screen: string | null) {
        if (screen != "" || screen != null)
            this.props.history.replace(screen);
    }

    ionViewWillEnter() {
        let outer = this;
        Promise.resolve(Storage.getToken()).then(function (token) {
            if (token.value != null && token.value != undefined) {
                outer.setState({ showLoading: false })
                outer.setState({token: token.value});
                outer.validateToken();
            } else {
                outer.setState({ token: null });
                console.log("No token avaiable"); 
            }
        }, function (value) {
            // not called
        });
    }

    validateToken() {
        let outer = this;
        if (this.state.token != null) {
            console.log(this.state.token);
            axios({
                method: 'post',
                url: `http://${Variables.API}/projetos/hivefy/api/Autenticacao/token/`,
                data: `token=${this.state.token}`
            })
                .then(function (response) {
                    console.log(response.data);
                    if(response.data.status == "valid"){
                        outer.mirrorTo("/home")
                    }
                })
                .catch(function (error) {
                    console.log('Erro:', error);
                })
                .finally(function () {

                });
        }
    }

    validateForm() {
        if (this.state.email == "" || this.state.email == null || this.state.email == " ") {
            this.setState({ showPopoverEmail: true })
        } else if (this.state.password == "" || this.state.password == null || this.state.password == " ") {
            this.setState({ showPopoverPassword: true })
        } else {
            try {
                this.setState({ showLoading: true })
                let outer = this;
                axios({
                    method: 'post',
                    url: `http://${Variables.API}/projetos/hivefy/api/Autenticacao/usuario/`,
                    data: `email=${this.state.email}&senha=${this.state.password}`
                })
                    .then(function (response) {
                        outer.setState({ showLoading: false })
                        console.log(response.data);
                        if (response.data.status == 'success') {
                            outer.setState({ showLoading: false })
                            console.log('success');
                            outer.setState({ token: response.data.token, uid: response.data.uid });
                            Storage.setStorageItem("token", response.data.token);
                            outer.validateToken();
                        } else if (response.data.status == 'nomatch') {
                            outer.setState({ showLoading: false, message: 'Senha incorreta', showAlert: true })
                            console.log('incorrect password');
                        } else if (response.data.status == 'notfound') {
                            outer.setState({ showLoading: false, message: 'Não encontramos nenhum usuário com o e-mail informado', showAlert: true })
                            console.log('user not found');
                        } else if (response.data.status == 'nullentry') {
                            outer.setState({ showLoading: false, message: 'Dados inválidos', showAlert: true })
                            outer.setState({ showLoading: false })
                            console.log('no info');
                        }
                        outer.setState({ showLoading: false })
                    })
                    .catch(function (error) {
                        outer.setState({ showLoading: false })
                        console.log('Erro:', error);
                    })
                    .finally(function () {
                        outer.setState({ showLoading: false })
                    });

            } catch (error) {

            }
        }
        this.setState({ showLoading: false })
    }

    render() {
        return (
            <IonPage>
                <IonContent className="main">
                    <div className="head flexy">
                        <div className="mu-header flexy">
                            <h1 className="mu-title">HIVEFY</h1>
                            <h2 className="mu-description">Conexão estudantil nunca foi tão fácil</h2>
                        </div>
                        <div className="mu-cover">
                        </div>
                    </div>
                    <div className="container flexy stretchy">
                        <div className="mu-body gridy">
                            <div className="espaco20"></div>
                            <h1 className="mu-title">LOGIN</h1>
                            <div className="espaco25"></div>
                            <IonLoading
                                message={'Encontrando suas coméias'}
                                isOpen={this.state.showLoading}
                                onDidDismiss={() => { this.setState({ showLoading: false }) }}
                            />
                            <IonAlert
                                isOpen={this.state.showAlert}
                                onDidDismiss={() => { this.setState({ showAlert: false }) }}
                                header={this.state.message}
                                buttons={[
                                    {
                                        text: 'Confirmar'
                                    }
                                ]}
                            />
                            <form className="gridy" action="">
                                <input type="email" className="mu-input" placeholder="E-mail" onChange={event => { this.setState({ email: event.target.value }) }} required></input>
                                <IonPopover isOpen={this.state.showPopoverEmail} onDidDismiss={e => { this.setState({ showPopoverEmail: false }) }}>
                                    <div className="mu-body text-center"><p> Preencha seu email</p></div>
                                </IonPopover>
                                <div className="espaco15"></div>
                                <input type="password" className="mu-input" placeholder="Senha" onChange={event => { this.setState({ password: event.target.value }) }} required></input>
                                <IonPopover isOpen={this.state.showPopoverPassword} onDidDismiss={e => { this.setState({ showPopoverPassword: false }) }}>
                                    <div className="mu-body text-center"><p> Preencha sua senha</p></div>
                                </IonPopover>
                                <div className="espaco25"></div>
                                <button type="submit" id="muAccess" className="mu-button" onClick={(e) => {
                                    e.preventDefault();
                                    this.validateForm();
                                }}><h4>ACESSAR</h4></button>
                            </form>
                            <div className="espaco10"></div>
                            <a onClick={(e) => { this.mirrorTo("/access") }}>Esqueceu sua senha?</a>
                            <div className="espaco20"></div>
                            <button className="mu-button secondary" onClick={(e) => { this.mirrorTo("/register") }}><h4>CADASTRAR</h4></button>
                            <div className="espaco10"></div>
                        </div>
                    </div>
                </IonContent>
            </IonPage>
        );
    }
};

export default withIonLifeCycle(Login);

// FILE: Tab1.tsx
import React, { useState } from 'react';
import { IonIcon, withIonLifeCycle } from '@ionic/react';
import * as Components from './components';
import { string } from 'prop-types';
import * as Icons from 'ionicons/icons';
import { Icon } from 'ionicons/dist/types/icon/icon';

interface PostProps {
    Title: string | null,
    Description: string | null,
    Image: string,
    Comments: string | null,
    Attachments: string | null,
    Id: string | null,
    Color: string | null,
    User: string,
    Group: string,
    DateTime: string,
    Likes: string,
}

const Post: React.FC<PostProps> = ({ Title, Description, Group, Image, User, Comments, Attachments, Id, Color, DateTime, Likes }) => {
    
    const [_Liked, setLiked] = useState(false);
    const [_Likes, setLikes] = useState(Likes);

    function like(){
        if(_Liked)
            setLikes((parseInt(_Likes)-1).toString());
        else if(!_Liked)
            setLikes((parseInt(_Likes)+1).toString());
            setLiked(!_Liked);
    }

    return (
        <>
            <Components.PostWrapper className={"Post"} borderColor={Color}>
                <Components.Post30>
                    <Components.PostImage profilePicture={Image} borderColor={Color}></Components.PostImage>
                </Components.Post30>
                <Components.Post70>
                    <Components.UserTitle>{User} <Components.PostSmall lightColor={Color}>{Group}</Components.PostSmall></Components.UserTitle>
                    <Components.PostDate>{DateTime}</Components.PostDate>
                </Components.Post70>
                <Components.Post100>
                    <Components.PostTitle>{Title}</Components.PostTitle>
                    <Components.PostDescription>{Description}</Components.PostDescription>
                </Components.Post100>
                <Components.Post33>
                    <Components.PostIcon Icon={Icons.attach}></Components.PostIcon>    
                    <Components.PostIconTitle>
                        {Attachments}
                    </Components.PostIconTitle>
                </Components.Post33>
                <Components.Post33>
                    <Components.PostIcon Icon={Icons.chatbubbles}></Components.PostIcon>    
                    <Components.PostIconTitle>
                        {Comments}
                    </Components.PostIconTitle>
                </Components.Post33>
            </Components.PostWrapper>
        </>
    );
};

export default Post;
import styled from 'styled-components';
import React from 'react';
import { IonIcon, withIonLifeCycle } from '@ionic/react';
import gradstop from 'gradstop';

let Style = {
    Header: {
        "font-size": "40px",
        "color": "#FF6C6C",
    },

    Insider: {
        "font-size": "40px",
        "color": "#FF6C6C",
        'margin-left': 'auto'
    },

    Inner: {
        "font-size": "20px",
        "color": "white",
        'margin-left': 'auto'
    },

    GreyActivity: {
        "height": "20px",
        "width": "20px",
        "border-radius": "99px",
        "background-image": "linear-gradient(to left top, #6a6a6a, #8d8d8d, #b2b2b2, #d8d8d8, #ffffff) linear-gradient(to right, rgb(255, 91, 91), rgba(255, 108, 108), rgb(252, 134, 134), rgba(255, 255, 8))"
    },

    PatternActivity: {
        "height": "20px",
        "width": "20px",
        "border-radius": "99px",
        "background-image": "linear-gradient(to right, rgb(255, 91, 91), rgba(255, 108, 108), rgb(252, 134, 134), rgba(255, 255, 8))"
    }
}

/* Helpers */

export const Wrapper = styled.div` 
    padding-left: 15px;
    padding-right: 15px;
    overflow: scroll;
    min-height: 100%;
`;


/* Header */

export const HeaderWrapper = styled.div` 
    display: flex;
    margin-top: 16px;
    margin-bottom: 16px;
    align-items: center;
`;


export const HeaderTitle = styled.h3` 
    font-family: 'Nunito-Bold';
    color: #FF6C6C;
    margin-left: 4px
`;

const Icon = ({ className, children, Icon }) => (
    <IonIcon className={className} style={Style.Header} icon={Icon}>{children}
        {console.log(Icon)}</IonIcon>
);

export const HeaderIcon = styled(Icon)` 
    
`;

/* Post */

export const PostWrapper = styled.div` 
    display:flex;
    flex-wrap: wrap;
    margin-top: 16px;
    margin-bottom: 16px;
    align-items: center;
    padding: 20px;
    border: 1.3px solid ${props => props.borderColor || "#FF6C6C"};
    border-radius: 20px;
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
`;

export const Post30 = styled.div` 
    width: 30%;
`;

export const Post33 = styled.div` 
    width: 33.3%;
    margin-left:auto;
    margin-right:auto;
    display: flex;
    align-items: center;
`;

export const Post70 = styled.div` 
    width: 70%;
`;

export const Post100 = styled.div` 
    width: 100%;
`;

export const PostImage = styled.div` 
    height: 20vw;
    width: 20vw;
    max-width: 110px;
    max-height: 110px;
    margin-left:auto;
    margin-right:auto;
    background-image: url(${props => props.profilePicture});
    background-position: center;
    background-size: cover;
    border-radius: 99px;
    border: 1.3px solid;
    border-color: ${props => props.borderColor || "#FF6C6C"};
`;

export const UserTitle = styled.h3` 
    font-family: 'Nunito-Bold';
    color: #FF6C6C;
    margin: 0px;
`;

export const PostTitle = styled.h3` 
    font-family: 'Nunito-SemiBold';
    color: #FF6C6C;
`;

export const PostDate = styled.p` 
    font-family: 'Nunito-Light';
    color: grey;
    opacity: 0.5;
    margin: 0px;
`;

export const PostDescription = styled.p` 
    font-family: 'Nunito-Regular';
    color: #5C5C5C;
`;

export const PostSmall = styled.small` 
    font-family: 'Nunito-Light';
    color: ${props => props.lightColor || "#FF6C6C"};
    opacity: 0.5;
`;


const InsiderIcon = ({ className, children, Icon }) => (
    <IonIcon className={className} style={Style.Insider} icon={Icon}>{children}
        {console.log(Icon)}</IonIcon>
);

export const PostIcon = styled(InsiderIcon)` 
    
`;

export const PostIconTitle = styled.h3` 
    font-family: 'Nunito-SemiBold';
    color: #FF6C6C;
    margin-right: auto;
    margin-left: 5px;
`;


/* Hive Preview */
function LightenDarkenColor(col, amt) {

    var usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

}

function generateGradient(color) {
    console.log(color);
    let gradient = gradstop({ stops: 10, inputFormat: 'hex', colorArray: [LightenDarkenColor(color, -10), color,  LightenDarkenColor(color, 100)] });
    let result = ''
    gradient.forEach((gradientcolor, index) => {
        if (index < gradient.length - 1)
            result += gradientcolor + ','
        else
            result += gradient[index - 1]
    });
    return result;
}

let color;

export const HivePreviewWrapper = styled.div` 
    display:grid;
    flex-wrap: wrap;
    margin-top: 16px;
    margin-bottom: 16px;
    align-items: center;
    padding: 20px;
    width: 100%;
    height: auto;
    border: 1px solid transparent;
    border-radius: 20px;
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    background-image: linear-gradient(to bottom, ${props => generateGradient(props.innerColor)});
`;


export const HivePreviewTitle = styled.h2` 
    font-family: 'Nunito-SemiBold';
    margin-top: 1px;
    margin-bottom: 1px;
    color: white;
    display: flex;
`;


export const HivePreviewMini = styled.h6` 
    font-family: 'Nunito-Light';
    margin-top: 0px;
    margin-bottom: 0px;
    color: white;
`;

export const HivePreviewDescription = styled.h5` 
    font-family: 'Nunito-Regular';
    margin-top: 10px;
    margin-bottom: 10px;
    color: white;
`;

const InnerIcon = ({ className, children, Icon }) => (
    <IonIcon className={className} style={Style.Inner} icon={Icon}>{children}
        {console.log(Icon)}</IonIcon>
);

export const LockIcon = styled(InnerIcon)` 
    
`;


export const HivePreviewNewMessages = styled.div` 
    height: 20px;
    width: 20px;
    border-radius: 99px;
    background-image: ${props => props.HasNewMessages ? "linear-gradient(to right, rgb(255, 91, 91), rgba(255, 108, 108), rgb(252, 134, 134), rgba(255, 255, 8))" : "linear-gradient(to left top, #6a6a6a, #8d8d8d, #b2b2b2, #d8d8d8, #ffffff)"}
`;
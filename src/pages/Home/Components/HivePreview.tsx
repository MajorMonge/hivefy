// FILE: Tab1.tsx
import React, { useState } from 'react';
import { IonIcon, withIonLifeCycle } from '@ionic/react';
import * as Components from './components';
import { string } from 'prop-types';
import * as Icons from 'ionicons/icons';
import { Icon } from 'ionicons/dist/types/icon/icon';

interface HivePreviewProps {
    Id: string | null,
    Title: string | null,
    Description: string | null,
    CreatedAt: string | null,
    JoinedAt: string | null,
    CreatedBy: string | null,
    NewMessages: boolean | null,
    NewMessagesQuantity: string | null,
    Color: string | null,
    Locked: boolean | null,
}

const HivePreview: React.FC<HivePreviewProps> = ({
    Id,
    Title,
    Description,
    CreatedAt,
    JoinedAt,
    CreatedBy,
    NewMessages,
    NewMessagesQuantity,
    Color,
    Locked,
}) => {


    return (
        <>
            <Components.HivePreviewWrapper className={"Post"} innerColor={Color} >
                <Components.HivePreviewTitle>{Title}
                    <Components.LockIcon Icon={Locked ? Icons.lock : Icons.unlock}></Components.LockIcon>
                </Components.HivePreviewTitle>
                <Components.HivePreviewMini>Criado em {CreatedAt} por {CreatedBy}</Components.HivePreviewMini>
                <Components.HivePreviewMini>Você ingressou nessa colméia em {JoinedAt}</Components.HivePreviewMini>
                <Components.HivePreviewDescription>{Description}</Components.HivePreviewDescription>
                <div style={{"display":"flex"}}>
                    <Components.HivePreviewNewMessages HasNewMessages={NewMessages}></Components.HivePreviewNewMessages>
                    <Components.HivePreviewMini  style={{"margin-left":"5px"}}>{NewMessages ? NewMessagesQuantity + " novas postagens" : "Sem novas postagens"}
                    </Components.HivePreviewMini>
                </div>
            </Components.HivePreviewWrapper>
        </>
    );
};

export default HivePreview;
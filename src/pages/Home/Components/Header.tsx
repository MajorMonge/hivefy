// FILE: Tab1.tsx
import React from 'react';
import { IonIcon, withIonLifeCycle } from '@ionic/react';
import * as Components from './components';
import { string } from 'prop-types';

interface HeaderProps {
    Title: string
    Icon: object
}

const Header: React.FC<HeaderProps> = ({ Title, Icon }) => {
    return (
        <>
            <Components.HeaderWrapper>
                <Components.HeaderIcon Icon={Icon}></Components.HeaderIcon>
                <Components.HeaderTitle>
                    {Title}
                </Components.HeaderTitle>
            </Components.HeaderWrapper>
        </>
    );
};

export default Header;
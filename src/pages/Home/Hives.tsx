// FILE: Tab1.tsx
import React from 'react';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonPage, withIonLifeCycle } from '@ionic/react';
import * as Components from './Components/components';
import Header from './Components/Header'
import HivePreview from './Components/HivePreview'
import * as Icons from 'ionicons/icons';

class Hives extends React.Component<any, any> {

    render() {
        return (
            <>
                <IonPage id="main">
                    <Components.Wrapper>
                        <Header Title={"Coméias"} Icon={Icons.contacts}></Header>
                        <div>
                            <HivePreview
                                Id                      ={""}
                                Title                   ={"Lorem impsum sit"}
                                Description             ={"Lorem impsum sit amet, et al also tunis ponis"}
                                CreatedAt               ={"25/10/2019"}
                                JoinedAt                ={"23/11/2019"}
                                CreatedBy               ={"Carlos Meningue"}
                                NewMessages             ={true}
                                NewMessagesQuantity     ={"5"}
                                Color                   ={"#50c421"}
                                Locked                  ={null}
                            >
                            </HivePreview>
                        </div>
                    </Components.Wrapper>
                </IonPage>
                <IonContent />
            </>
        );
    }
};

export default withIonLifeCycle(Hives);
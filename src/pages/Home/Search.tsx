// FILE: Tab1.tsx
import React from 'react';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonPage, withIonLifeCycle } from '@ionic/react';
import * as Components from './Components/components';
import Header from './Components/Header'
import * as Icons from 'ionicons/icons';

class Search extends React.Component<any, any> {

    render() {
        return (
            <>
                <IonPage id="main">
                    <Components.Wrapper>
                        <Header Title={"Busca"} Icon={Icons.search}></Header>
                    </Components.Wrapper>
                </IonPage>
                <IonContent />
            </>
        );
    }
};

export default withIonLifeCycle(Search);
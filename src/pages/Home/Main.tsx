import React from 'react';
import { IonHeader, IonList, IonItem, IonToolbar, IonTitle, IonContent, IonPage, withIonLifeCycle } from '@ionic/react';
import * as Components from './Components/components';
import Header from './Components/Header'
import Post from './Components/Post'
import * as Icons from 'ionicons/icons';

class Main extends React.Component<any, any> {

    render() {
        return (
            <>
                <IonPage id="main">
                    <Components.Wrapper>
                        <Header Title={"Início"} Icon={Icons.home}></Header>
                        <Post
                            Id={null}
                            Image={'https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350'}
                            User={'Eugenio diz'}
                            Group={'em BCC 2019'}
                            DateTime={'22/04/2020'}
                            Title={'Lorem impsum sit amet descript'}
                            Description={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam semper lectus ac mauris dictum, suscipit mattis augue bibendum. Suspendisse finibus turpis ac neque consectetur tincidunt. Mauris non sollicitudin nulla. Pellentesque venenatis in metus non fringilla...'}
                            Likes={'5'}
                            Comments={'2'}
                            Attachments={'1'}
                            Color={'orange'}></Post>
                        <Post
                            Id={null}
                            Image={'https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350'}
                            User={'Eugenio diz'}
                            Group={'em BCC 2019'}
                            DateTime={'22/04/2020'}
                            Title={'Lorem impsum sit amet descript'}
                            Description={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam semper lectus ac mauris dictum, suscipit mattis augue bibendum. Suspendisse finibus turpis ac neque consectetur tincidunt. Mauris non sollicitudin nulla. Pellentesque venenatis in metus non fringilla...'}
                            Likes={'5'}
                            Comments={'2'}
                            Attachments={'1'}
                            Color={'lime'}></Post>
                        <Post
                            Id={null}
                            Image={'https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350'}
                            User={'Eugenio diz'}
                            Group={'em BCC 2019'}
                            DateTime={'22/04/2020'}
                            Title={'Lorem impsum sit amet descript'}
                            Description={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam semper lectus ac mauris dictum, suscipit mattis augue bibendum. Suspendisse finibus turpis ac neque consectetur tincidunt. Mauris non sollicitudin nulla. Pellentesque venenatis in metus non fringilla...'}
                            Likes={'5'}
                            Comments={'2'}
                            Attachments={'1'}
                            Color={'blue'}></Post>
                    </Components.Wrapper>
                </IonPage>
                <IonContent />
            </>
        );
    }
};

export default withIonLifeCycle(Main);
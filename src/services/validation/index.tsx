
import Storage from '../storage';
import Variables from '../../variables';
import axios from 'axios';

async function validateToken() {
    return new Promise(resolve => {
        let token: string | null = null;

        Promise.resolve(Storage.getToken()).then(function (storagetoken) {
            if (storagetoken.value != null && storagetoken.value != undefined) {
                token = storagetoken.value;
            } else {
                resolve(false);
            }

            axios({
                method: 'post',
                url: `http://${Variables.API}/projetos/hivefy/api/Autenticacao/token/`,
                data: `token=${token}`
            })
                .then(function (response) {
                    if (response.data.status == "valid") {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                })
                .catch(function (error) {
                    resolve(false);
                })
                .finally(function () {

                });
        });

    })
}

export default { validateToken };
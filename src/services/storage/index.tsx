
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;


async function setStorageItem(itemname: string, value: string) {
    if (itemname == "token") {
        await Storage.set({
            key: 'token',
            value: value
        });
    }
}

async function removeStorageItem(itemname: string){
    await Storage.remove({
        key: itemname
    })
}

async function getToken() {
    const value = await Storage.get({ key: 'token' });
    return value;
}

export default { setStorageItem, getToken, removeStorageItem };